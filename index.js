// https://konghq.com/blog/how-to-design-a-scalable-rate-limiting-algorithm/
const express = require("express");
const pg = require("pg");
const redis = require("redis");

// https://gist.github.com/woozyking/126fdf4c72fdf65a3504e5681a1ce715
// https://www.npmjs.com/package/express-limiter
const app = express();

const dotenv = require("dotenv");
dotenv.config();
// console.log('process.env :: ', process.env);

// configs come from standard PostgreSQL env vars
// https://www.postgresql.org/docs/9.6/static/libpq-envars.html
const pool = new pg.Pool();
const { REDIS_HOST, REDIS_PORT, REDIS_PASSWORD } = process.env;
client = redis.createClient({
  host: REDIS_HOST,
  port: REDIS_PORT,
  password: REDIS_PASSWORD
});

// console.log('redis client :: ', client);

const queryHandler = (req, res, next) => {
  pool
    .query(req.sqlQuery)
    .then(r => {
      return res.json(r.rows || []);
    })
    .catch(next);
};

const rateLimiter = (req, res, next) => {
  console.log("rate limiter", req, res);
  next();
};

app.use(rateLimiter);
// TODO: create express middleware that threshold user request on their IP address of given time period
// e.g: INCOMING IP = 108.1.3.2
//      EXPIRATION = 1HR
//      MAX CONNECTIONS = 100

app.get(
  "/",
  (req, res) => {
    res.send("Welcome to EQ Works 😎");
  },
  rateLimiter
);

app.get(
  "/events/hourly",
  (req, res, next) => {
    req.sqlQuery = `
    SELECT date, hour, events
    FROM public.hourly_events
    ORDER BY date, hour
    LIMIT 168;
  `;
    return next();
  },
  queryHandler
);

app.get(
  "/events/daily",
  (req, res, next) => {
    req.sqlQuery = `
    SELECT date, SUM(events) AS events
    FROM public.hourly_events
    GROUP BY date
    ORDER BY date
    LIMIT 7;
  `;
    return next();
  },
  queryHandler
);

app.get(
  "/stats/hourly",
  (req, res, next) => {
    req.sqlQuery = `
    SELECT date, hour, impressions, clicks, revenue
    FROM public.hourly_stats
    ORDER BY date, hour
    LIMIT 168;
  `;
    return next();
  },
  queryHandler
);

app.get(
  "/stats/daily",
  (req, res, next) => {
    req.sqlQuery = `
    SELECT date,
        SUM(impressions) AS impressions,
        SUM(clicks) AS clicks,
        SUM(revenue) AS revenue
    FROM public.hourly_stats
    GROUP BY date
    ORDER BY date
    LIMIT 7;
  `;
    return next();
  },
  queryHandler
);

app.get(
  "/poi",
  (req, res, next) => {
    req.sqlQuery = `
    SELECT *
    FROM public.poi;
  `;
    return next();
  },
  queryHandler
);

app.listen(process.env.PORT || 5555, err => {
  if (err) {
    console.error(err);
    process.exit(1);
  } else {
    console.log(`Running on ${process.env.PORT || 5555}`);
  }
});

// last resorts
process.on("uncaughtException", err => {
  console.log(`Caught exception: ${err}`);
  process.exit(1);
});
process.on("unhandledRejection", (reason, p) => {
  console.log("Unhandled Rejection at: Promise", p, "reason:", reason);
  process.exit(1);
});
